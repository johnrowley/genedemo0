﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GitDemoNe0.Startup))]
namespace GitDemoNe0
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
